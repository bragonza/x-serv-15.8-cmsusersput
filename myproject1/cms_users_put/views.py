from django.shortcuts import render
from .models import Contenido
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect
# Create your views here.

formulario = """
<p><form action="" method= "POST">
    Valor: <input type="text" name="valor"><br/>
    <input type="submit" value="Enviar">
</form>
"""

@csrf_exempt
def get_content(request, llave):
    if request.user.is_authenticated:
        if request.method == "PUT":
            valor = request.body.decode('utf-8')
            try:
                c = Contenido.objects.get(clave=llave)
                c.valor = valor #Actualizar valor
            except Contenido.DoesNotExist:
                c = Contenido(clave=llave, valor=valor)
            c.save()   #Guardarlo en la base de datos

        if request.method == "POST":
        #Coger el valor que haya en el cuerpo de HTTP
            valor = request.POST['valor']
            try:
                c = Contenido.objects.get(clave=llave)
                c.valor = valor  # Actualizar valor
            except Contenido.DoesNotExist:
                c = Contenido(clave=llave, valor=valor)
            c.save()  # Guardarlo en la base de datos

        try:
            contenido = Contenido.objects.get(clave=llave)
            respuesta = "El valor de " + contenido.clave + " es " + contenido.valor + ", y su id es " + str(
            contenido.id) + "<br>Cambiar valor: " + formulario
        except Contenido.DoesNotExist:
            respuesta = "Eres " + request.user.username + "<br>"
            respuesta += "No existe contenido para " + llave + "<br>Añadelo: " + formulario
        respuesta += "<a href='/cms/logout'>Cerrar sesión"
    else:
        contenido = Contenido.objects.get(clave=llave)
        respuesta = "El valor de " + contenido.clave + " es " + contenido.valor + ", y su id es " + str(
            contenido.id)
        respuesta += "<br>Necesitas autentificarte para cambiar su valor. <a href='/login'>Logearse"
    return HttpResponse(respuesta)

def index(request):
    if request.user.is_authenticated:
        if request.method == "PUT":
            valor = request.body.decode('utf-8')

            try:
                c = Contenido.objects.get(clave=llave)
                c.valor = valor #Actualizar valor
            except Contenido.DoesNotExist:
                c = Contenido(clave=llave, valor=valor)
            c.save()   #Guardarlo en la base de datos

        if request.method == "POST":
        #Coger el valor que haya en el cuerpo de HTTP
            llave = request.POST['llave']
            valor = request.POST['valor']
            try:
                c = Contenido.objects.get(clave=llave)
                c.valor = valor  # Actualizar valor
            except Contenido.DoesNotExist:
                c = Contenido(clave=llave, valor=valor)
            c.save()  # Guardarlo en la base de datos

        content_list = Contenido.objects.all()
        autenticado = request.user.is_authenticated
        template = loader.get_template('cms/index.html')
        context = {
            'content_list': content_list,
            'authenticate': autenticado,
        }
        respuesta = template.render(context, request)
    else:
        content_list = Contenido.objects.all()
        autenticado = request.user.is_authenticated
        template = loader.get_template('cms/index.html')
        context = {
            'content_list': content_list,
            'authenticate': autenticado,
        }
        respuesta = template.render(context, request)
        respuesta += "<br>Necesitas autentificarte para cambiar los contenidos. <a href='/login'>Logearse"
    return HttpResponse(respuesta)

def LoggedIn(request):
    if request.user.is_authenticated:
        respuesta = "Logged in as " + request.user.username
    else:
        respuesta = "Not logged in. <a href='/admin/'>Login vía admin</a>"
    return HttpResponse(respuesta)
def logout_view(request):
    logout(request)
    return redirect("/login")  #redirije a login
# Create your views here.
